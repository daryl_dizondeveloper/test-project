<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App Dizon</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>    
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Test Project</h1>
            <hr class="my-4">
            <p>Note: To input multiple ID - make it one ID per line.</p>
        </div>
        <form method="POST" action="{{ route('popcash-add-site') }}">
            @csrf
            <div class="form-group">
                <label>Campaign ID/s</label>
                <textarea name="ids" class="form-control" placeholder="Campaign ID/s" rows="5" required>{{ $ids ?? '' }}</textarea>        
            </div>
            <div class="form-group">
                <label>Website ID/s</label>
                <textarea name="websites" class="form-control" placeholder="Website ID/s" rows="5" required>{{ $websites ?? '' }}</textarea>        
            </div>
            <div class="form-group">
                <input type="submit" value="Submit" class="btn btn-success" />
            </div>
        </form>
        @if(isset($result))
        <div class="alert alert-{{ $alert }}" role="alert">
            @if(isset($output))
                {{ 'Output: ' . $output }}<br /><br />
            @endif

            @if($result == 'success')
                @if(isset($campaignIds))
                    Campaign Ids:<br />
                    @foreach($campaignIds as $item)
                        {{ $item }}<br />
                    @endforeach
                @endif
                <br />
                @if(isset($websiteIds))
                    Website Ids:<br />
                    @foreach($websiteIds as $item)
                        {{ $item }}<br />
                    @endforeach
                @endif
            @else
                {{ $result }}
            @endif
        </div>
        @endif
    </div>    
</body>
</html>