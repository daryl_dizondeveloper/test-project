<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/public', '/');
Route::get('/', function () { return view('home'); })->name('home');
Route::get('/get/{id}', 'Api\PopCashController@GetWebsitesById');

Route::post('/', 'Api\PopCashController@AddSiteId')->name("popcash-add-site");
