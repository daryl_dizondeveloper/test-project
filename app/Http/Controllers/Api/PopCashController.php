<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PopCashController extends Controller
{
    // 146853 blacklist
    // 152354
    // 187003 blacklist
    // 123099
    public function AddSiteId(Request $request)
    {
        $ids = $request->input('ids'); // Campaign ID/s
        $websites = $request->input('websites'); // Website ID/s

        $campaignIdsString = explode(PHP_EOL, $ids);
        $campaignIds = array_map(
            function($value) { return (int)$value; },
            $campaignIdsString
        );
        $websiteIdsString = explode(PHP_EOL, $websites);
        $websiteIds = array_map(
            function($value) { return (int)$value; },
            $websiteIdsString
        );

        if($this->IsInvalidId($campaignIds, $websiteIds)) {
            return view('home')
                ->with('ids', $ids)
                ->with('websites', $websites)
                ->with('alert', 'danger')
                ->with('result', 'Error Message: You input an invalid campaign/website id.');
        }

        $hasWhitelist = false;
        foreach ($campaignIds as $id) {
            if($this->IsWhitelisted($id)) {
                $hasWhitelist = true;
                break;
            }
        } 
        
        if($hasWhitelist) {
            return view('home')
                ->with('ids', $ids)
                ->with('websites', $websites)
                ->with('alert', 'danger')
                ->with('result', 'Error Message: You input a campaign id that used a whitelist.');
        }

        foreach ($campaignIds as $id) {

            $apiKey = env('POPCASH_API_KEY');
            $body = [];
            $body['append'] = true;
            $body['siteTargeting'] = 1;
            $body['websitesIds'] = $websiteIds;

            $response = Http::withHeaders([
                'X-Api-Key' => $apiKey,
                'Content-Type' => 'application/json'
            ])->post(str_replace('{id}', $id, 'https://api.popcash.net/campaigns/{id}/targeting'), $body);
        }        

        // return $response->json();
        return view('home')
                ->with('ids', $ids)
                ->with('websites', $websites)
                ->with('alert', 'success')
                ->with('campaignIds', $campaignIds)
                ->with('websiteIds', $websiteIds)
                ->with('result', 'success');
    }

    private function IsWhitelisted($id) 
    {
        if($id)
        $apiKey = env('POPCASH_API_KEY');
        $response = Http::withHeaders([
            'X-Api-Key' => $apiKey,
            'Content-Type' => 'application/json'
        ])->get(str_replace('{id}', $id, 'http://api.popcash.net/campaigns/{id}/targeting'));
        
        $result = $response->json();
        if($result["type"] == 2) {
            return true;
        }

        return false;
    }
    private function IsInvalidId($campaignIds, $websiteIds) 
    {
        $IsInvalid = false;
        foreach ($campaignIds as $id) {
            if(!is_int($id) || $id == 0) {
                $IsInvalid = true;
                break;
            }
        }        
        foreach ($websiteIds as $id) {
            if(!is_int($id) || $id == 0) {
                $IsInvalid = true;
                break;
            }
        } 

        return $IsInvalid;
    }

    public function GetWebsitesById($id) 
    {
        $apiKey = env('POPCASH_API_KEY');
        $response = Http::withHeaders([
            'X-Api-Key' => $apiKey,
            'Content-Type' => 'application/json'
        ])->get(str_replace('{id}', $id, 'http://api.popcash.net/campaigns/{id}/targeting'));
        
        return $response->json();
    }
}
